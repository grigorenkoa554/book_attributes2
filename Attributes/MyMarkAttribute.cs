﻿using System;

namespace Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class MyMarkAttribute : Attribute
    {
        public MyMarkAttribute()
        {

        }
        public MyMarkAttribute(int count)
        {

        }
        public int MyProperty { get; set; }
    }
}
