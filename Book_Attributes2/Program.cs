﻿using Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;

[assembly: CLSCompliant(true)]
namespace Book_Attributes2
{
    class Program
    {
        static void Main(string[] args)
        {
            ulong ii = 23;
            Console.WriteLine(ii);
            var attrs = typeof(Some).CustomAttributes;
            foreach (var attr in attrs)
            {
                Console.WriteLine("Atribute - {0}", attr.AttributeType);
                foreach (var item1 in attr.ConstructorArguments)
                {
                    Console.WriteLine("Arg of ctor - {0}", item1.Value);
                }
            }
            Console.WriteLine("=======================================================");
            var path = "../../../../MyRunClass/bin/Debug/netcoreapp3.1/MyRunClass.dll";
            var assambly = Assembly.LoadFrom(path);
            Run(assambly);
        }
        public static void Run(Assembly assambly)
        {
            foreach (var type in assambly.GetTypes())
            {
                if (type.IsClass && type.CustomAttributes.Any(x => x.AttributeType == typeof(MyMarkAttribute)))
                {
                    var instance = Activator.CreateInstance(type);
                    var methods = type.GetMethods()
                        .Where(x => x.GetCustomAttributes()
                            .Any(x => x.GetType() == typeof(MyMarkAttribute)));
                    foreach (var methodInfo in methods)
                    {
                        var attribute = methodInfo.CustomAttributes
                            .Single(x => x.AttributeType == typeof(MyMarkAttribute));
                        //var attrs2 = (from c in methodInfo.GetCustomAttributes(false)
                        //                where (c is MyMarkAttribute)
                        //                select c).Cast<MyMarkAttribute>().ToList();
                        //attrs2[0].MyProperty
                        var countAttribute = attribute.ConstructorArguments.FirstOrDefault();
                        if (countAttribute != null)
                        {
                            var count = (int)countAttribute.Value;
                            for (int i = 0; i < count; i++)
                            {
                                methodInfo.Invoke(instance, null);
                            }
                        }
                    }
                }
            }
        }

    }

    [Obsolete("sdfsdf")]
    [Table("sdf")]
    [MyMark(1, MyProperty = 2)]
    public class Some
    {
        public int MyProperty { get; set; }
        public uint Some2 { get; set; }

    }
}
