﻿using Attributes;
using System;

namespace MyRunClass
{
    [MyMark]
    public class Calculate
    {
        public static int count = 0;
        [MyMark(3)]
        public void Print()
        {
            count++;
            Console.WriteLine($"Method Print use {count} times..");
        }
    }
}
